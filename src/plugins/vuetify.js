import '@fortawesome/fontawesome-free/css/all.css'
import 'vue-toastr-2/dist/vue-toastr-2.min.css'
import Vue from 'vue';
import Vuetify from 'vuetify/lib/framework';

Vue.use(Vuetify);

export default new Vuetify({
	icon: {
		iconFont: 'fa'
	}
});
