import 'vue-toastr-2/dist/vue-toastr-2.min.css'

import Vue from 'vue'
import App from './App.vue'
import vuetify from './plugins/vuetify'
import router from '@/config/Routes'

import axios from 'axios';
import store from '@/store'

import VueToastr2 from 'vue-toastr-2'
import vueNumeralFilterInstaller from 'vue-numeral-filter';
import moment from 'moment';
import VueCurrencyInput from 'vue-currency-input'
import excel from 'vue-excel-export'

const pluginOptions = {
  globalOptions: { currency: 'USD',}
}

Vue.use(excel)
Vue.use(VueCurrencyInput, pluginOptions)

import { VueMaskDirective } from 'v-mask'
Vue.directive('mask', VueMaskDirective);

Vue.use(vueNumeralFilterInstaller);

// Date format
Vue.filter('formatDate', function(value, format="MM/DD/YYYY hh:mm:ss") {
    if (value) {
        return moment(String(value)).format(format)
    }
});

Vue.config.productionTip = false

Vue.prototype.urlBase = process.env.NODE_ENV === 'production' ? 'http://api.administracion.lucatienda.com/api': 'api';

Vue.prototype.$http = axios.create();
Vue.prototype.$http.defaults.headers.common['Authorization'] = 'Bearer '+store.state.token

Vue.prototype.$http.interceptors.response.use(undefined, function(error) {
  if (error.response.status === 401 && error.config && !error.config.__isRetryRequest) {
    store.dispatch('logout');
    router.push({name: 'Login'});
  } else {
    console.log(error)
    return new Promise((resolve,reject) => {

      if (error.response.status === 500) {
        reject('Estamos teniendo inconvenientes con el servidor, inténtalo más tarde.');
      } else if (error.response.status === 404) {
        reject('No se encontró el servicio solicitado, inténtalo más tarde.');
      } else {
        reject(error);
      }
    });
  }
});
window.toastr = require('toastr')
Vue.use(VueToastr2)

new Vue({
  vuetify,
  router,
  store,
  render: h => h(App)
}).$mount('#app')
