export default {
	name: "Action",
	data: () =>({
		btnSave: false,
		btnSearch: false,
		selfSearch: false,
		startArray: false,
		seeForm: false,
		form: {},
		bform: {},
		list: [],
		reg: '',
		service:'',
		text: '',
		startDate: "",
		endingDate: "",
		catalogo: {},
		key: null
	}),
	mounted() {
		if (this.selfSearch) {
			this.search();
		}
	},
	methods: {
		search() {
			this.btnSearch = true
			this.$http
			.get(`${this.urlBase}/${this.service}`, {params: this.bform})
			.then(r => {
				this.list = r.data.list
				this.btnSearch = false
			}).catch(e => {
				console.log(e)
				this.btnSearch = false
			})
		},
		requestGet(service) {
			return new Promise((resolve, reject) => {
				this.$http
				.get(`${this.urlBase}/${service}`, {params: this.bform})
				.then(r => {
					return resolve(r)
				}).catch(e => {
					console.log(e)
					return reject(e)
				})
			})
			
		},
		save(form) {
			this.btnSave = true;
			this.$http
			.post(`${this.urlBase}/${this.service}/${this.reg}`, this.form)
			.then(r => {
				if (r.data.success) {
					this.$toastr.success(r.data.message)
					if (this.reg == '') {
						this.clean();
						if(form){
							form.resetValidation()
						}
					}
					if (r.data.record) {
						if (this.reg == '') {
							if (this.startArray) {
								this.list.unshift(r.data.record);
							} else {
								this.list.push(r.data.record);
							}
						}  else {
							/*let tmp = this.list.filter(e => {
								return e.id == this.reg
							})[0];

							if (tmp) {*/
								//let key = this.list.indexOf(tmp);
								for (let i in this.list[this.key]) {
									this.list[this.key][i] = r.data.record[i];
								}
							//}
						}
					}
				} else {
					this.$toastr.error(r.data.message)
				}
				this.btnSave = false;
			})
			.catch(e => {
				this.$toastr.error(e)
				this.btnSave = false;
			});
		},
		remove(obj) {
				this.btnSave = true;
				this.$http
				.delete(`${this.urlBase}/${this.service}/${obj.id}`)
				.then(r => {
					if (r.data.success) {
						this.$toastr.success(r.data.message)
						let key = this.list.indexOf(obj);
						this.list.splice(key, 1);
					} else {
						this.$toastr.error(r.data.message)
					}
					this.btnSave = false;
				}).catch(e => {
					this.btnSave = false;
					console.log(e)
				})
			
		},
		clean () {
			this.reg = '';
			this.form = {};
			this.key = null;
		},
		edit (idx) {
			this.key = idx;
			let tmp = this.filtered[idx];

			this.reg = tmp.id;
			for (let i in tmp) {
				this.form[i] = tmp[i];
			}
			this.seeForm = true;
			if (this.scrollTo) {
				window.scrollTo(0,0);
			}
		},
		getSpValues(sp="", obj={}, idx="") {
			const data = {
				sp: sp,
				object: obj
			}

			this.$http
			.post(`${this.urlBase}/values`, data)
			.then(r => {
				this.$set(this.catalogo, (idx || sp), r.data.data)
			}).catch(e => {
				console.log(`SP: ${sp}, Error: ${e}`)
			})
		},
		getDate() {
			let tmp = new Date();

			return ('0' + tmp.getDate()).slice(-2) + '/' + ('0' + (tmp.getMonth()+1)).slice(-2) + '/' + tmp.getFullYear();
		},
		dateFormatInput(date) {
			let [year, month, day] = date.split('-')

			return `${day}/${month}/${year}`;
		}
	},
	computed: {
		filtered() {
			return this.list
		}
	}
}