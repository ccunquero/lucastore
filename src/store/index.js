import Vue from 'vue';
import Vuex from 'vuex';
import createPersistedState from "vuex-persistedstate";
import createdMutationsSharer from "vuex-shared-mutations";

Vue.use(Vuex)

export default new Vuex.Store({
	state: {
		token: '',
		user: {},
		active: false,
		company: null
	},
	mutations: {
		setToken(state, value) {
			state.token = value;
		},
		setUser(state, value) {
			state.user = value;
		}
	},
	actions: {
		login(state, data) {

			return new Promise((resolve, reject) => {

				Vue.prototype.$http
				.post(Vue.prototype.urlBase+'/session/login', data)
				.then(res => {
					if (res.data.success) {
						state.commit('setUser', res.data.user);
						state.commit('setToken', res.data.token);

						Vue.prototype.$http.defaults.headers.common['Authorization'] = 'Bearer '+state.state.token;
					} 
					resolve(res.data);
				}).catch(e => {
					reject(e)
				})
			})
		},
		logout(state) {
			state.commit('setToken', null);
			state.commit('setUser', null);
			delete Vue.prototype.$http.defaults.headers.common['Authorization']
		}
	},
	modules: {
	},
	getters: {
		isLoggedIn: state => !!state.token,
		company: state => state.user ? state.user.userCompanies[0].company: {},
		user: state => state.user
	},
	plugins:[
		createPersistedState({key: "lucastore"}),
		createdMutationsSharer({predicate: ['setToken','setUser']})
	]
})